
local component = require("component")
local bridge = component.openperipheral_bridge
bridge.clear()
bridge.sync()

local event = require("event")

local _users = bridge.getUsers()
local users = {}
for i,v in pairs(_users) do
	users[v.uuid] = {name = v.name, uuid = v.uuid, capt = bridge.getCaptureControl(v.uuid), slots={}}
end

local render = {}

function render.items(user)
	print("items")
	local canvas = user.canvas
	local ids = user.ids

	ids.box1 = canvas.addBox(-100,-100,200,220,0xcccccc,0.5)
	ids.box1.setScreenAnchor("MIDDLE","MIDDLE")

	local slots = {}


	local first = true
	for i=0,9 do
	--    local line = canvas.addLine({i*20+2,-100},{i*20+2,100,0x000000,0.8})
	--    line.setScreenAnchor("MIDDLE","MIDDLE")
	--    line.setWidth(6)
	--    line.setZ(2)

	--    local line = canvas.addLine({i,-100},{i,100})

		for j=0,10 do
			local box = canvas.addBox(-100+i*20,-100+j*20,18,18,i == 0 and 0xffdddd or 0xdddddd,0.5)
			box.setScreenAnchor("MIDDLE","MIDDLE")
		end
	end
end


function draw(user)
	if not user.canvas then
		user.canvas = bridge.getSurfaceByName(user.name)
		user.canvas.clear()
		user.ids = {}
	end
	if render[user.state] then
		render[user.state](user)
	else
		user.canvas.clear()
	end
end

local events = {"glasses_attach","glasses_chat_command","interrupted","glasses_capture","glasses_release"}

local handlers = {}

function handlers.glasses_attach(_,_,name,uuid)
	if users[uuid] then return end
	users[uuid] = {uuid=uuid,name=name,capt = bridge.getCaptureControl(uuid), slots={}}
end

function handlers.glasses_chat_command(_,_,name,uuid,com)
	if not users[uuid] then users[uuid] = {name=name,uuid=uuid,capt = bridge.getCaptureControl(uuid), slots={}} end
	if users[uuid].state == com then
		users[uuid].state = ""
	else
		users[uuid].state = tostring(com)
	end
end

function handlers.glasses_capture(_,_,_,uuid)
	if users[uuid] then
		users[uuid].capture = true
		users[uuid].capt.toggleGuiElements({[1]=true,[2]=false,[3]=false,[4]=false,[5]=false,[6]=false,[7]=false,[8]=false,[9]=false,[10]=false,[11]=false,[12]=false,[13]=false})
		users[uuid].capt.setBackground(0x000000,0)
	end
end

function handlers.glasses_release(_,_,_,uuid)
	if users[uuid] then
		users[uuid].capture = false
	end
end

local run = true

function handlers.interrupted()
	for i,v in pairs(users) do
		if v.canvas then v.canvas.clear() end
	end
	bridge.clear()
	bridge.sync()
	run = false
end

while run do
	local evt = {event.pullMultiple(table.unpack(events))}
	print(evt[1])
	if handlers[evt[1]] then
		handlers[evt[1]](table.unpack(evt))
	end
	if not run then break end
	for i,v in pairs(users) do draw(v) end
	bridge.sync()
end